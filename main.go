package main

import (
	"fmt"
	"log"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
)

const (
	host     = "localhost"
	port     = 5432
	user     = "postgres"
	password = "postgres"
	dbname   = "challenge"
)

type Snippet struct {
	Name string `json:"name"`
	Text string `json:"text"`
}

type NewSnippetRequest struct {
	OwnerID string `json:"ownerId"`
	Snippet
}

func newPostgresqlConnection() *sqlx.DB {
	db, err := sqlx.Open("postgres", fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable", host, port, user, password, dbname))
	if err != nil {
		panic(err.Error())
	}
	if err = db.Ping(); err != nil {
		panic(err.Error())
	}
	return db
}

func main() {
	postgres := newPostgresqlConnection()
	app := fiber.New()
	app.Use(cors.New())

	app.Get("/snippets", func(c *fiber.Ctx) error {
		var snippets []Snippet
		if err := postgres.Select(&snippets, "SELECT name, text FROM snippets"); err != nil {
			return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
				"message": err.Error(),
			})
		}

		return c.JSON(fiber.Map{
			"snippets": snippets,
		})
	})
	app.Post("/snippets", func(c *fiber.Ctx) error {
		var newSnippet NewSnippetRequest
		if err := c.BodyParser(&newSnippet); err != nil {
			return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
				"message": err.Error(),
			})
		}
		tx, err := postgres.Begin()
		if err != nil {
			return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
				"message": err.Error(),
			})
		}

		tx.Exec("INSERT INTO snippets (name, text, owner_id) VALUES ($1, $2, $3)", newSnippet.Name, newSnippet.Text, newSnippet.OwnerID)
		err = tx.Commit()
		if err != nil {
			return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
				"message": err.Error(),
			})
		}
		return c.SendStatus(fiber.StatusOK)
	})

	log.Fatal(app.Listen(":3000"))
}
