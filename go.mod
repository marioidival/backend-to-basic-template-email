module gitlab.com/marioidival/backend-to-basic-template-email

go 1.16

require (
	github.com/gofiber/fiber/v2 v2.20.0
	github.com/jmoiron/sqlx v1.3.4
	github.com/lib/pq v1.2.0
)
