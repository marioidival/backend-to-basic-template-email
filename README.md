# Backend to Basic Template Email


## Technologies

- **Golang**: web frawework (go-fiber)
- **PostgreSQL**

## Endpoints

### GET /snippets

Return a list of snippets saved

```
☁  backend-to-basic-template-gmail [main] ⚡ https challenge.marioidival.xyz/snippets
HTTP/1.1 200 OK
Access-Control-Allow-Origin: *
Connection: keep-alive
Content-Length: 208
Content-Type: application/json
Date: Wed, 06 Oct 2021 09:34:41 GMT
Server: nginx/1.18.0 (Ubuntu)
Vary: Origin

{
    "snippets": [
        {
            "name": "welcome",
            "text": "this text is about welcome to the company"
        },
        {
            "name": "report",
            "text": "this text is about new reports"
        },
        {
            "name": "challenge",
            "text": "this text is about coding challenge"
        }
    ]

```

### POST /snippets

Create a new snippet


```
☁  backend-to-basic-template-gmail [main] echo '{"name": "presentation", "text": "this text is about Golang presentation"}' | http POST https://challenge.marioidival.xyz/snippets
HTTP/1.1 200 OK
Access-Control-Allow-Origin: *
Connection: keep-alive
Content-Length: 2
Content-Type: text/plain; charset=utf-8
Date: Wed, 06 Oct 2021 09:37:30 GMT
Server: nginx/1.18.0 (Ubuntu)
Vary: Origin

OK


☁  backend-to-basic-template-gmail [main] https challenge.marioidival.xyz/snippets
HTTP/1.1 200 OK
Access-Control-Allow-Origin: *
Connection: keep-alive
Content-Length: 280
Content-Type: application/json
Date: Wed, 06 Oct 2021 09:37:36 GMT
Server: nginx/1.18.0 (Ubuntu)
Vary: Origin

{
    "snippets": [
        {
            "name": "welcome",
            "text": "this text is about welcome to the company"
        },
        {
            "name": "report",
            "text": "this text is about new reports"
        },
        {
            "name": "challenge",
            "text": "this text is about coding challenge"
        },
        {
            "name": "presentation",
            "text": "this text is about Golang presentation"
        }
    ]
}
```
